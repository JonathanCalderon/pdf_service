var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var FileReader = require('filereader')
var SCOPES = ['https://www.googleapis.com/auth/drive'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
  process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'drive-nodejs-quickstart.json';
console.log(TOKEN_PATH)
var auth = '';
var googleDrive = require('google-drive');

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Drive API.
  authorize(JSON.parse(content), function(resp) {
    auth = resp;
    listFiles(auth);
  });
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Lists the names and IDs of up to 10 files.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listFiles(auth) {
  var service = google.drive('v3');
  service.files.list({
    auth: auth,
    pageSize: 10,
    fields: "nextPageToken, files(id, name)"
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var files = response.files;
    if (files.length == 0) {
      console.log('No files found.');
    } else {
      console.log('Files:');
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        console.log('%s (%s)', file.name, file.id);
      }
    }
  });
}


function insertFile(path, prefijo, tipo, sufijo) {

  var drive = google.drive({
    version: 'v3',
    auth: auth
  });

  findFile(prefijo + '-' + tipo, function(err, res) {
    if (err)
      return console.error(err);
    console.log('inserFile res', res);
    if (res && res.length > 0) {
      var folderId = res[0].id;
      var fileMetadata = {
        'name': prefijo + '-' + sufijo,
        parents: [folderId]
      };
      var media = {
        mimeType: 'application/pdf',
        body: fs.createReadStream(path)
      };
      drive.files.create({
        resource: fileMetadata,
        media: media,
        fields: 'id'
      }, function(err, file) {
        if (err) {
          // Handle error
          console.error(err);
        } else {
          console.log('File Id: ', file.id);
          return 'File Id: ', file.id;
        }
      });
    } else {
      console.log('NO hay carpeta ', res);
      createFolder(prefijo + '-' + tipo, function(err2, folderId) {
        if (err2)
          return console.error(err2);
        var fileMetadata = {
          'name': prefijo + '-' + sufijo,
          parents: [folderId]
        };
        var media = {
          mimeType: 'application/pdf',
          body: fs.createReadStream(path)
        };
        drive.files.create({
          resource: fileMetadata,
          media: media,
          fields: 'id'
        }, function(err, file) {
          if (err) {
            // Handle error
            console.error(err);
          } else {
            console.log('File Id: ', file.id);
            return 'File Id: ', file.id;
          }
        });

      });
    }

  });

  /*
    drive.files.insert({
      resource: {
        title: 'bussiness.pdf',
        mimeType: 'application/pdf'
      },
      media: {
        mimeType: 'application/pdf',
        body: fs.createReadStream(path) // read streams are awesome!
      }
    }, function(err, resp) {
      if (err)
        console.error(err);
    });
  */
}


function createFolder(name, callback) {
  var drive = google.drive({
    version: 'v3',
    auth: auth
  });
  var fileMetadata = {
    'name': name,
    'mimeType': 'application/vnd.google-apps.folder'
  };
  drive.files.create({
    resource: fileMetadata,
    fields: 'id'
  }, function(err, file) {
    if (err) {
      callback(err);
    } else {
      callback(null, file.id);
    }
  });


}

function findFile(folderName, callback) {
  var drive = google.drive({
    version: 'v3',
    auth: auth
  });
  console.log('foldername ', folderName);
  drive.files.list({
    q: "mimeType='application/vnd.google-apps.folder' and name=\'" + folderName + "\'",
    spaces: 'drive'
  }, function(err, res) {
    console.log(res);
    if (err || !res || !res.files) {
      console.log('error');
      return console.error('No hay carpeta');
    }
    console.log('Find folder', res);
    return callback(null, res.files);
  });
}

module.exports = {
  listFiles: listFiles,
  insertFile: insertFile,
  findFile: findFile
};