var http = require('http');
var jsreport = require('jsreport');
var express = require('express');
var router = express.Router();
var fs = require('fs');
var pdf = '';
var htmlToPdf = require('html-pdf');
var googleDrive = require('../google-drive');

var nodeUtil = require("util"),
  _ = require('underscore'),
  PDFParser = require("../node_modules/pdf2json/pdfparser");



router.get('/', function(req, res) {

  jsreport.render(pdf).then(function(out) {
    out.stream.pipe(res);
  }).catch(function(e) {
    res.end(e.message);
  });

});

router.post('/', function(req, res) {

  var jsons = req.body;
  prepararJSON(jsons);
  console.log('jsons' + JSON.stringify(jsons));
  res.send('Cargado');
});

router.post('/getLink', function(req, res) {

  var html = req.body;
  console.log('params ', req.headers);
  var data = req.headers.data;
  var path = req.headers.path;
  var filename = req.headers.filename;
  var folderpath = req.headers.folderpath;
  pdf = html;
  var date = new Date();
  var filenameDate = '' + date.getFullYear() + (date.getMonth() + 1) + date.getDate() +
    date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
  jsreport.render({
    template: {
      content: html,
      engine: 'jsrender',
      recipe: 'phantom-pdf'
    },
    data: data
  }).then(function(out) {

    var finalname = './public/pdf/' + filenameDate + '.pdf';
    var isPublic = false;
    if (path)
      finalname = path;

    else if (filename) {
      finalname = './public/pdf/' + filename + '.pdf';
      isPublic = true;
    } else if (folderpath) {
      finalname = folderpath + filenameDate + '.pdf';
    } else {
      isPublic = true;
      filename = filenameDate;
    }

    setTimeout(function() {
      out.stream.pipe(fs.createWriteStream(finalname));

      if (isPublic)
        res.send('http://localhost:3000/pdf/' + filename + '.pdf');

      else
        res.send('Su archivo ha sido guardado en la siguiente ruta: ' + finalname);

    }, 5000);

  }).catch(function(e) {
    res.end(e.message);
  });
});

router.post('/getPDF', function(req, res) {

  var html = req.body;
  console.log('params ', req.headers);
  var data = req.headers.data;
  pdf = html;
  var date = new Date();
  jsreport.render({
    template: {
      content: html,
      engine: 'jsrender',
      recipe: 'phantom-pdf'
    },
    data: data
  }).then(function(out) {
    out.stream.pipe(res);
  }).catch(function(e) {
    res.end(e.message);
  });
});

router.post('/getLink2', function(req, res) {

  var self = this;
  var html = req.body;
  var prefijo = req.headers.prefijo;
  var tipo = req.headers.tipo;
  var sufijo = req.headers.sufijo;
  var filename = prefijo + '-' + sufijo;
  console.log(prefijo, tipo, sufijo);
  var options = {
    format: 'Letter',
    "border": {
      "top": "2cm", // default is 0, units: mm, cm, in, px 
      "right": "1cm",
      "bottom": "2cm",
      "left": "1cm"
    },

    name: 'juan'

  };

  htmlToPdf.create(html, options).toFile('./public/pdf/' + filename + '.pdf', function(err, resp) {
    if (err) return console.log(err);
    console.log(resp); // { filename: '/app/businesscard.pdf' } 

    googleDrive.insertFile('./public/pdf/' + filename + '.pdf', prefijo, tipo, sufijo);

    res.send('http://localhost:3000/pdf/' + filename + '.pdf');
  });

});

router.post('/getPDF2', function(req, res) {

  var html = req.body;
  var options = {
    format: 'Letter',
    "border": {
      "top": "2cm", // default is 0, units: mm, cm, in, px 
      "right": "1cm",
      "bottom": "2cm",
      "left": "1cm"
    }
  };

  htmlToPdf.create(html, options).toStream(function(err, stream) {
    if (err) return console.log(err);
    stream.pipe(res);
  });

});

router.post('/htmlToPDF', function(req, res) {

  var html = req.body;
  var path = req.headers.path;

  console.log(html);
  console.log('path: ', path);
  var options = {
    format: 'Letter',
    "border": {
      "top": "2cm", // default is 0, units: mm, cm, in, px 
      "right": "1cm",
      "bottom": "2cm",
      "left": "1cm"
    }, // File options
    "type": "pdf", // allowed file types: png, jpeg, pdf

    // Script options
    "phantomPath": "./node_modules/phantomjs/bin/phantomjs", // PhantomJS binary which should get downloaded automatically
    "phantomArgs": [],
    "phantom": {
      numberOfWorkers: 4
    }
  };
  htmlToPdf.create(html, options).toFile(path, function(err, resp) {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }
    return res.send(resp);
  });
});


router.post('/python', function(req, res) {

  console.log(req);
  console.log('body ', req.body);
  res.send(req.body);
});

router.post('/json', function(req, res) {

  var html = req.body.html;
  var data = req.body.data;
  var date = new Date();
  var filename = '' + date.getFullYear() + (date.getMonth() + 1) + date.getDate() +
    date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
  jsreport.render({
    template: {
      content: html,
      engine: 'jsrender',
      recipe: 'phantom-pdf'
    },
    data: data
  }).then(function(out) {
    //prints pdf with headline Hello world
    out.stream.pipe(fs.createWriteStream('./public/pdf/' + filename + '.pdf'));
    res.send('http://localhost:3000/pdf/' + filename + '.pdf');
  });
});

function _onPFBinDataReady(resp) {

  console.log('_onPFBinDataReady')
  googleDrive.insertFile2(resp, function(resp2) {
    console.log(resp2);
  });
}

function _onPFBinDataError(resp) {

  console.error(resp);
}

/* Report generator
require('jsreport')({
  httpPort: 2000
}).init();
*/
module.exports = router;